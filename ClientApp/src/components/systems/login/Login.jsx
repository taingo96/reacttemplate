import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import './login.css'

export class Login extends Component {

    constructor(props) {
        super(props);

        const token = localStorage.getItem("token");
        let isLogin = true;
        if (token == null) {
            isLogin = false;
        }

        this.state = {
            username: "",
            password: "",
            isLogin: isLogin
        }
        
        this.onChange = this.onChange.bind(this);
        this.onLogin = this.onLogin.bind(this);
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onLogin() {
        const { username, password } = this.state;
        if (username === "test" && password === "123") {
            localStorage.setItem("token", this.createToken());
            this.setState({
                isLogin: true
            });
        }
    }

    rand() {
        return Math.random().toString(36).substr(2);
    };

    createToken() {
        return this.rand() + this.rand();
    };

    render() {
        if (this.state.isLogin) {
            return <Redirect to="/" />
        }
        return (
            <div className="login-wrap">
                <div className="login-html">
                    <input id="tab-1" type="radio" name="tab" className="sign-in" defaultChecked /><label htmlFor="tab-1" className="tab">Sign In</label>
                    <input id="tab-2" type="radio" name="tab" className="sign-up" /><label htmlFor="tab-2" className="tab">Sign Up</label>
                    <div className="login-form">
                        <div className="sign-in-htm">
                            <div className="group">
                                <label htmlFor="user" className="label">Username</label>
                                <input name="username" type="text" className="input" value={this.state.username} onChange={this.onChange} />
                            </div>
                            <div className="group">
                                <label htmlFor="pass" className="label">Password</label>
                                <input name="password" type="password" className="input" data-type="password" value={this.state.password} onChange={this.onChange} />
                            </div>
                            <div className="group">
                                <input id="check" type="checkbox" className="check" defaultChecked />
                                <label htmlFor="check"><span className="icon" /> Keep me Signed in</label>
                            </div>
                            <div className="group">
                                <input type="button" className="button" defaultValue="Sign In" onClick={this.onLogin} />
                            </div>
                            <div className="hr" />
                            <div className="foot-lnk">
                                <a href="#forgot">Forgot Password?</a>
                            </div>
                        </div>
                        <div className="sign-up-htm">
                            <div className="group">
                                <label htmlFor="user" className="label">Username</label>
                                <input id="user" type="text" className="input" />
                            </div>
                            <div className="group">
                                <label htmlFor="pass" className="label">Password</label>
                                <input id="pass" type="password" className="input" data-type="password" />
                            </div>
                            <div className="group">
                                <label htmlFor="pass" className="label">Repeat Password</label>
                                <input id="pass" type="password" className="input" data-type="password" />
                            </div>
                            <div className="group">
                                <label htmlFor="pass" className="label">Email Address</label>
                                <input id="pass" type="text" className="input" />
                            </div>
                            <div className="group">
                                <input type="submit" className="button" defaultValue="Sign Up" />
                            </div>
                            <div className="hr" />
                            <div className="foot-lnk">
                                <label htmlFor="tab-1">Already Member?</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
