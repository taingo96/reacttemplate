import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor(props) {
    super(props);

    this.state = { isLogin: true }

    this.logOut = this.logOut.bind(this);

  }

  componentDidMount() {

  }

  logOut() {
    localStorage.removeItem("token");
    this.setState({
      isLogin: false
    })
  }

  render() {
    if (!this.state.isLogin) {
      return <Redirect to="/login" />
    }
    return (
      <div className="wrapper">
        <header className="header-top" header-theme="light">
          <div className="container-fluid">
            <div className="d-flex justify-content-between">
              <div className="top-menu d-flex align-items-center">
                <button type="button" className="btn-icon mobile-nav-toggle d-lg-none"><span /></button>
              </div>
              <div className="top-menu d-flex align-items-center">
                <div className="dropdown">
                  <a className="nav-link dropdown-toggle" id="notiDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="ik ik-bell" /><span className="badge bg-danger">3</span></a>
                  <div className="dropdown-menu dropdown-menu-right notification-dropdown" aria-labelledby="notiDropdown">
                    <h4 className="header">Notifications</h4>
                    <div className="notifications-wrap">
                      <a className="media">
                        <span className="d-flex">
                          <i className="ik ik-check" />
                        </span>
                        <span className="media-body">
                          <span className="heading-font-family media-heading">Invitation accepted</span>
                          <span className="media-content">Your have been Invited ...</span>
                        </span>
                      </a>
                      <a className="media">
                        <span className="d-flex">
                          <img src="public/assets/images/users/1.jpg" className="rounded-circle" alt="" />
                        </span>
                        <span className="media-body">
                          <span className="heading-font-family media-heading">Steve Smith</span>
                          <span className="media-content">I slowly updated projects</span>
                        </span>
                      </a>
                      <a className="media">
                        <span className="d-flex">
                          <i className="ik ik-calendar" />
                        </span>
                        <span className="media-body">
                          <span className="heading-font-family media-heading">To Do</span>
                          <span className="media-content">Meeting with Nathan on Friday 8 AM ...</span>
                        </span>
                      </a>
                    </div>
                    <div className="footer"><a>See all activity</a></div>
                  </div>
                </div>
                <div className="dropdown">
                  <a className="dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img className="avatar" src="assets/images/user.jpg" alt="" /></a>
                  <div className="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a className="dropdown-item" href="profile.html"><i className="ik ik-user dropdown-icon" /> Profile</a>
                    <a className="dropdown-item"><i className="ik ik-settings dropdown-icon" /> Settings</a>
                    <a className="dropdown-item"><span className="float-right"><span className="badge badge-primary">6</span></span><i className="ik ik-mail dropdown-icon" /> Inbox</a>
                    <a className="dropdown-item"><i className="ik ik-navigation dropdown-icon" /> Message</a>
                    <a className="dropdown-item" onClick={this.logOut} ><i className="ik ik-power dropdown-icon" /> Logout</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
        <div className="page-wrap">
          <div className="app-sidebar colored">
            <div className="sidebar-header">
              <a className="header-brand" href="index.html">
                <div className="logo-img">
                  <img src="assets/images/brand-white.svg" className="header-brand-img" alt="lavalite" />
                </div>
                <span className="text">ThemeKit</span>
              </a>
              <button type="button" className="nav-toggle"><i data-toggle="expanded" className="ik ik-toggle-right toggle-icon" /></button>
              <button id="sidebarClose" className="nav-close"><i className="ik ik-x" /></button>
            </div>
            <div className="sidebar-content">
              <div className="nav-container">
                <nav id="main-menu-navigation" className="navigation-main">
                  <div className="nav-lavel">Other</div>
                  <div className="nav-item has-sub">
                    <a ><i className="ik ik-list" /><span>Menu Levels</span></a>
                    <div className="submenu-content">

                      <Link to="/home" className="menu-item">Home</Link>
                      <div className="nav-item has-sub">
                        <Link to="/counter" className="menu-item" >Counter</Link>
                        <div className="submenu-content">
                          <a className="menu-item">Menu Level 3.1</a>
                        </div>
                      </div>
                      <Link to="/fetch-data" className="menu-item">FetchData</Link>

                    </div>
                  </div>
                  <div className="nav-item">
                    <a className="disabled"><i className="ik ik-slash" /><span>Disabled Menu</span></a>
                  </div>
                  <div className="nav-item">
                    <a ><i className="ik ik-award" /><span>Sample Page</span></a>
                  </div>
                </nav>
              </div>
            </div>
          </div>
          <div className="main-content">
            <div className="container-fluid">
              <Container>
                {this.props.children}
              </Container>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
