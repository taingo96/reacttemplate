import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { NavMenu } from './NavMenu';
import { Home } from '../features/Home';
import { FetchData } from '../features/FetchData/FetchData';
import { Counter } from '../features/Counter';

export class Layout extends Component {
  constructor(props) {
    super(props);
    const token = localStorage.getItem("token");

    let isLogin = true;
    if (token == null) {
      isLogin = false;
    }

    this.state = { isLogin }
  }

  render() {
    if (!this.state.isLogin) {
      return <Redirect to="/login" />
    }
    return (
      <div>
        <NavMenu>
          < Route path='/home' component={Home} />
          < Route path='/counter' component={Counter} />
          < Route path='/fetch-data' component={FetchData} />
        </NavMenu>
      </div>
    );
  }
}
