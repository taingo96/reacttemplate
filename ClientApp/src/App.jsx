import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { Login } from './components/systems/login/Login';
import { Layout } from './components/systems/Layout';

export default class App extends Component {
    static displayName = App.name;
    
    render() {
            return (
                <Switch>
                    < Route exact path='/login' component={Login} />
                    < Route path='/' component={Layout} />
                </Switch >
            );
    }
}